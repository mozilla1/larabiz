<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class ListingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'address' => 'required',
            'website' => 'required',
            'phone' => 'required',
            'bio' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El campo nombre es requerido',
            'email.required' => 'El campo correo es requerido',
            'address.required' => 'El campo direccion es requerido',
            'website.required' => 'El campo website es requerido',
            'phone.required' => 'El campo telefono es requerido',
            'bio.required' => 'El campo bio es requerido',
        ];
    }
}
