@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Panel de lista <span class="float-right"><a href="/home" class="btn btn-secondary">Regresar</a></span></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="/listings/{{$listing->id }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Nombre" value="{{ $listing->name }}">
                        </div>

                        <div class="form-group">
                            <label for="name">Direccion</label>
                            <input type="text" name="address" id="address" class="form-control" placeholder="Direccion" value="{{ $listing->address }}">
                        </div>

                        <div class="form-group">
                            <label for="name">Website</label>
                            <input type="text" name="website" id="website" class="form-control" placeholder="Website" value="{{ $listing->website }}">
                        </div>

                        <div class="form-group">
                            <label for="name">Correo</label>
                            <input type="text" name="email" id="email" class="form-control" placeholder="Correo" value="{{ $listing->email }}">
                        </div>

                        <div class="form-group">
                            <label for="name">Telefono</label>
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="Telefono" value="{{ $listing->phone }}">
                        </div>

                        <div class="form-group">
                            <label for="name">Bio</label>
                            <input type="text" name="bio" id="bio" class="form-control" placeholder="Bio" value="{{ $listing->bio }}">
                        </div>

                        <button type="submit"  name="submit" class="btn btn-info float-left">Enviar</button>
                    </form>
                </div>
            </div>
        </div>

@endsection
