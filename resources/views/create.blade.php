@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Crear listado <span class="float-right"><a href="/home" class="btn btn-secondary">Cancelar</a></span></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="/listings">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Nombre">
                        </div>

                        <div class="form-group">
                            <label for="name">Direccion</label>
                            <input type="text" name="address" id="address" class="form-control" placeholder="Direccion">
                        </div>

                        <div class="form-group">
                            <label for="name">Website</label>
                            <input type="text" name="website" id="website" class="form-control" placeholder="Website">
                        </div>

                        <div class="form-group">
                            <label for="name">Correo</label>
                            <input type="text" name="email" id="email" class="form-control" placeholder="Correo">
                        </div>

                        <div class="form-group">
                            <label for="name">Telefono</label>
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="Telefono">
                        </div>

                        <div class="form-group">
                            <label for="name">Bio</label>
                            <input type="text" name="bio" id="bio" class="form-control" placeholder="Bio">
                        </div>

                        <button type="submit"  name="submit" class="btn btn-info float-left">Enviar</button>
                    </form>
                </div>
            </div>
        </div>

@endsection



